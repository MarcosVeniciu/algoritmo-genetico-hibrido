import os
import random
import math


def AGH(tamanho_populacao, Quantidade_geracoes, problema):
    problema_analizado = problema
    populacao = []

    populacao = PopulacaoInicial(tamanho_populacao, problema_analizado)

    for geracao in range(0, Quantidade_geracoes):

        populacao = Cruzamento(populacao, tamanho_populacao)
        populacao = Mutacao(populacao, problema_analizado)
        populacao = Selecao(populacao, problema_analizado, tamanho_populacao)
    melhor_individuo = MelhorResultado(populacao, problema_analizado)
    return melhor_individuo


# gera uma populacao inical de individuos validos
def PopulacaoInicial(tamanho_populacao, problema_analizado):
    populacao_inicial = []
    individuo = []


    for indicie_individuo in range(0, tamanho_populacao):

        individuo.clear() # Garantir que o individuo vai estar sempre vazio
        if problema_analizado == 1:
            raiz_2 = math.sqrt(2)
            p = 2
            alfa = 2

            # lopp usado pra garantir que as variaveis encontradas estao dentro dos limites inferior e superior
            while True:

                # variaveis iniciais
                variavel_1 = NumeroAleatorioUniform()# valor entre 0 e 1
                variavel_2 = NumeroAleatorioUniform()

                # verifica se atendem as restriçoes
                restricao_1 = ( (raiz_2 * variavel_1 +  variavel_2) / ( raiz_2 * math.pow(variavel_1, 2) + 2*variavel_1*variavel_2 ) ) * p - alfa
                restricao_2 = ( variavel_2 / ( raiz_2 * math.pow(variavel_1, 2) + 2*variavel_1*variavel_2 ) ) * p - alfa
                restricao_3 = ( 1 / (raiz_2 * variavel_2 +  variavel_1 ) ) * p - alfa



                if ((restricao_1 <= 0) and (restricao_2 <= 0) and (restricao_3 <= 0)):
                    break
            # Adiciona os genes ao individuo
            individuo.append(variavel_1)
            individuo.append(variavel_2)
        else: # problema analizado é igual a dois

            # lopp usado pra garantir que as variaveis encontradas estao dentro dos limites inferior e superior
            while True:

                # variaveis iniciais
                variavel_1 = 0.0625 * NunAleatorio(1601) # A variavel sera um valor entre 0 e 100
                variavel_2 = 0.0625 * NunAleatorio(1601)
                variavel_3 = random.uniform(10, 201) # Gera um numero maior ou igual 10 e menor ou igual a 200
                variavel_4 = random.uniform(10, 201)

                # verifica se aterndem as restricoes
                restricao_1 = -variavel_1 + 0.0193*variavel_3
                restricao_2 = -variavel_2 + 0.00954*variavel_3
                restricao_3 = -math.pi*math.pow(variavel_3,2)*variavel_4 - (3/4)*math.pi*math.pow(variavel_3,3) + 1296000
                restricao_4 =  variavel_4 - 240

                if ( (restricao_1 <= 0) and (restricao_2 <= 0) and (restricao_3 <= 0) and (restricao_4 <= 0) ):
                    break

            # Adiciona os genes ao individuo
            individuo.append(variavel_1)
            individuo.append(variavel_2)
            individuo.append(variavel_3)
            individuo.append(variavel_4)

        # Adiciona uma copia do individuo valido na Populacao
        populacao_inicial.append(individuo.copy())

    return populacao_inicial

def Cruzamento(populacao, tamanho_populacao):

    # Variaveis ajustaveis
    probabilidade_herdar = 0.5

    #Variaveis Auxiliares
    nova_populacao = []
    individuo_1 = []
    individuo_2 = []
    filho_1 = []
    filho_2 = []

    nova_populacao.clear()
    while (len(populacao) >= 2):

        # Seleciona individuo 1
        indice_individuo_1 = NunAleatorio(len(populacao))
        individuo_1 = populacao[indice_individuo_1]  # pega um individuo aleatorio da Populacao
        nova_populacao.append(individuo_1.copy()) # insere na nova populacao
        del populacao[indice_individuo_1] # remove o individuo da populacao pra nao repeti-lo

        # Seleciona individuo 2
        indice_individuo_2 = NunAleatorio(len(populacao))
        individuo_2 = populacao[indice_individuo_2]
        nova_populacao.append(individuo_2.copy())
        del populacao[indice_individuo_2]


        # Esvazia os genes dos filhos para não ser adicionados genes extras
        filho_1.clear()
        filho_2.clear()

        # Cruzamento
        for indice_gene in range(0, len(individuo_1)):

            # Gera os filhos
            if NumeroAleatorioUniform() <=  probabilidade_herdar:
                #filho_1 recebe o gene do individuo_1
                filho_1.append(individuo_1[indice_gene])
                filho_2.append(individuo_2[indice_gene])
            else:
                #filho_1 recebe gene do individuo_2
                filho_1.append(individuo_2[indice_gene])
                filho_2.append(individuo_1[indice_gene])

        # Insere os filhos na nova geracao
        nova_populacao.append(filho_1.copy())
        nova_populacao.append(filho_2.copy())

    # Como o cruzamento termina quando a populacao tiver menos de 2 individuos, se ainda restar alguem entao
    # a populacao sera > 0 e < 2 logo igual 1
    if len(populacao) > 0:
        nova_populacao.append(populacao[0].copy())
    return nova_populacao

# Gera um nova populacao com individuos mutantes
def Mutacao(populacao, problema_analizado):
    individuo_mutante = []
    novos_mutantes = []

    for individuo in populacao:
        if(NumeroAleatorioUniform() < 0.5): # individuos que sofreram a mutacao
            individuo_mutante = Pertubacao(individuo, problema_analizado)
            novos_mutantes.append(individuo_mutante.copy())
        else: # individuos que não sera mutado
            novos_mutantes.append(individuo.copy())
    return novos_mutantes

# retorna o individuo com a melhor resultado da função objetivo
def MelhorResultado(populacao, problema_analizado):
    melho_individuo = []
    melhor_individuo = populacao[0] #pega o primeiro individuo da populacao

    #compara o primeiro individuo da populacao com todos os outros
    for individuo in populacao :
        #se algum individuo da populacao for melhor que o melho_individuo o individuo da populacao passa a ser o melhor_individuo
        if FuncaoObjetivo(individuo, problema_analizado) < FuncaoObjetivo(melhor_individuo, problema_analizado):
            melhor_individuo = individuo

    return melhor_individuo

# retorna a populacao que possui apenas os indiciduos que venceram os torneios
def Selecao(populacao, problema_analizado, tamanho_populacao):
    nova_geracao = []
    individuo_selecionado = []

    for i in range(0, tamanho_populacao):# o numero de tornerios é igual ao tamanho da populacao
        individuo_selecionado = Torneio(populacao, problema_analizado)
        nova_geracao.append(individuo_selecionado.copy())

    return nova_geracao

# retorna o individuo que venceu o torneio entre 3
def Torneio(populacao, problema_analizado):
    melhor_individuo = []
    competidor = []

    tamanho_torneio = 3
    indice = NunAleatorio(len(populacao))
    melhor_individuo = populacao[indice] # um individuo da populacao  pertencente ao indice

    for cont in range(1, tamanho_torneio):# 0 1 2 = tamanho 3
        indice_competidor = NunAleatorio(len(populacao))
        competidor = populacao[indice_competidor]

        if FuncaoObjetivo(competidor, problema_analizado) < FuncaoObjetivo(melhor_individuo, problema_analizado):
            melhor_individuo = competidor

    return melhor_individuo

def Pertubacao(individuo, problema_analizado):
    mutante = []
    gene_mutante = 0

    for indice_gene in range(0, len(individuo)):
        if problema_analizado == 1: # Problema 1

            while True: # vai ficar em loop enquanto a variavel estiver fora dos limites inferior e superior

                alfa = NumeroAleatorioUniform() # numero entre 0 e 1
                U = NumeroAleatorioUniform()
                limite_superior = 1
                limite_inferior = 0

                mutacao = alfa * (limite_superior - limite_inferior) * (2 * U - 1)

                gene_mutante = individuo[indice_gene] + mutacao
                if ((gene_mutante >= 0) and (gene_mutante <= 1)):
                    break

        else: # Problema 2
            # Limite superior e inferior para as variaveis 1 e 2 com indice 0 e 1 respectivamente
            if ((indice_gene == 0) or (indice_gene == 1)):
                limite_superior = 100
                limite_inferior = 0


                while True:   # vai ficar em loop enquanto a variavel estiver fora dos limites inferior e superior

                    alfa = NumeroAleatorioUniform()
                    U = NumeroAleatorioUniform()

                    mutacao = 0.0625 * (alfa * (limite_superior - limite_inferior) * (2 * U - 1))
                    gene_mutante = individuo[indice_gene] + mutacao
                    if ((gene_mutante >= 0) and (gene_mutante <= 100)):
                        break

            # Limite superior e inferior para as variaveis 3 e 4 com indice 2 e 3 respectivamente
            if ((indice_gene == 2) or (indice_gene == 3)):

                limite_superior = 10
                limite_inferior = 200

                while True: # vai ficar em loop enquanto a variavel estiver fora dos limites inferior e superior

                    alfa = NumeroAleatorioUniform()
                    U = NumeroAleatorioUniform()
                    # Calculo da pertubacao
                    mutacao = alfa * (limite_superior - limite_inferior) * (2 * U - 1)

                    gene_mutante = individuo[indice_gene] + mutacao
                    if ((gene_mutante >= 10) and (gene_mutante <= 200)):
                        break

        mutante.append(gene_mutante)

    return mutante

# retorna um numero aleatorio maior ou igual a 0 e menor que final_intervalo
def NunAleatorio(final_intervalo):
    return random.randrange(0, final_intervalo)

# retorna um nomero aleatorio entre 0 e 1
def NumeroAleatorioUniform():
    return random.uniform(0,1)

# Retorna o valor da funcao objetivo
def FuncaoObjetivo(individuo, problema_analizado):
    valor_fitness = 0

    if problema_analizado == 1:
        l = 100
        valor_fitness = ((2 * math.sqrt(individuo[0])) + individuo[1] ) * l
        valor_fitness = valor_fitness + Penalidade(individuo, problema_analizado)

    else: # problema 2

        valor_fitness = (0.6224 * individuo[0] * individuo[2] * individuo[3]) + (1.7781 * individuo[1] * math.pow(individuo[2], 2) ) + (3.1661 * math.pow(individuo[0], 2) * individuo[3] ) + (19.84 * math.pow(individuo[0], 2) * individuo[2] )

        valor_fitness = valor_fitness + Penalidade(individuo, problema_analizado)

    return valor_fitness

# Retorna o valor da penalidade por violar as restricoes
def Penalidade(individuo, problema_analizado):

    # Variaveis ajustaveis
    constante_penalizacao = 2

    if problema_analizado == 1:

        raiz_2 = math.sqrt(2)
        p = 2
        alfa = 2

        restricao_1 = ( (raiz_2 * individuo[0] +  individuo[1]) / ( raiz_2 * math.pow(individuo[0], 2) + 2*individuo[0]*individuo[1] ) ) * p - alfa
        restricao_2 = ( individuo[1] / ( raiz_2 * math.pow(individuo[0], 2) + 2*individuo[0]*individuo[1] ) ) * p - alfa
        restricao_3 = ( 1 / (raiz_2 * individuo[1] +  individuo[0] ) ) * p - alfa

        penalizacao = constante_penalizacao * ( max(0,restricao_1) + max(0,restricao_2) + max(0,restricao_3) )

    else: # problema 2

        restricao_1 = -individuo[0] + 0.0193*individuo[2]
        restricao_2 = -individuo[1] + 0.00954*individuo[2]
        restricao_3 = -math.pi*math.pow(individuo[2],2)*individuo[3] - (3/4)*math.pi*math.pow(individuo[2],3) + 1296000
        restricao_4 =  individuo[3] - 240

        penalizacao = constante_penalizacao * ( max(0,restricao_1) + max(0,restricao_2) + max(0,restricao_3) + max(0,restricao_4) )

    return penalizacao

def CalculoRestricoes(individuo, problema):

    if problema == 1:

        raiz_2 = math.sqrt(2)
        p = 2
        alfa = 2

        restricao_1 = ( (raiz_2 * individuo[0] +  individuo[1]) / ( raiz_2 * math.pow(individuo[0], 2) + 2*individuo[0]*individuo[1] ) ) * p - alfa
        restricao_2 = ( individuo[1] / ( raiz_2 * math.pow(individuo[0], 2) + 2*individuo[0]*individuo[1] ) ) * p - alfa
        restricao_3 = ( 1 / (raiz_2 * individuo[1] +  individuo[0] ) ) * p - alfa

        print("problema 1")
        print("individuo")
        print(individuo)
        print("")
        print("Fitness:")
        print(FuncaoObjetivo(individuo, problema))
        print("")
        print("restricao 1")
        print(restricao_1)
        print("")
        print("restricao 2")
        print(restricao_2)
        print("")
        print("restricao 3")
        print(restricao_3)

    else: # problema 2

        restricao_1 = -individuo[0] + 0.0193*individuo[2]
        restricao_2 = -individuo[1] + 0.00954*individuo[2]
        restricao_3 = -math.pi*math.pow(individuo[2],2)*individuo[3] - (3/4)*math.pi*math.pow(individuo[2],3) + 1296000
        restricao_4 =  individuo[3] - 240

        print("problema 2")
        print("individuo")
        print(individuo)
        print("")
        print("Fitness:")
        print(FuncaoObjetivo(individuo, problema))
        print("")
        print("restricao 1")
        print(restricao_1)
        print("")
        print("restricao 2")
        print(restricao_2)
        print("")
        print("restricao 3")
        print(restricao_3)
        print("")
        print("restricao 4")
        print(restricao_4)



#########################################################################################################################################################
#########################################################################################################################################################
#########################################################################################################################################################
#########################################################################################################################################################





# Variaveis ajustaveis
numero_de_testes = 50
problema = 1
tamanho_populacao = 200
Quantidade_geracoes = 150

# Variaveis Auxiliares
solucao = []
melhor_solucao = []
fitness = 0
lista = []
somatorio = 0
media = 0


# Primeira execução i = 0
os.system('clear') or None
print("Populacao: 1")

solucao = AGH(tamanho_populacao, Quantidade_geracoes, problema)
fitness = FuncaoObjetivo(solucao, problema)
lista.append(fitness)
media = media + fitness
melhor_solucao = solucao

# os numero_de_testes - 1 execuçoes restantes
for i in range(1,numero_de_testes):

    solucao = AGH(tamanho_populacao, Quantidade_geracoes, problema)
    fitness = FuncaoObjetivo(solucao, problema)
    lista.append(fitness)
    media = media + fitness

    if FuncaoObjetivo(solucao, problema) < FuncaoObjetivo(melhor_solucao, problema):
        melhor_solucao = solucao

    os.system('clear') or None
    print("Populacao: ", (i+1))




minimo = min(lista)
maximo = max(lista)

media = media/numero_de_testes

for i in range(0,len(lista)):
    somatorio = somatorio + math.pow((lista[i] - media), 2)
desvio_padrao = math.sqrt(somatorio/len(lista))


print("")
print("Minimo:")
print(minimo)
print("")
print("Maximo: ")
print(maximo)
print("")
print("Media:")
print(media)
print("")
print("Desvio Padrao:")
print(desvio_padrao)
print("")
print("")
print("Resultado das restricoes do melhor resultado")
print("")
CalculoRestricoes(melhor_solucao, problema)
print("")
print("")


print("FIM")
